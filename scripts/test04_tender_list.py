# import requests
from parameterized import parameterized
import unittest, requests
from..api import log
from ..api.api_approve_trust import ApiApproveTrust
from ..api.api_register_login import ApiRegesiterLogin
from ..api.api_tender import ApiTenDer
from ..uitl import read_json, parser_html


class TestTenderList(unittest.TestCase):
    def setUp(self) -> None:
        self.session = requests.session()
        self.reg = ApiRegesiterLogin(self.session)
        self.approve = ApiApproveTrust(self.session)
        self.tender = ApiTenDer(self.session)

    def tearDown(self) -> None:
        self.session.close()

    def test01_tender_list(self):
        img_code = 8888
        phone_code = 666666
        phone = 13299151968
        pwd = "test123"

        # 获取图片验证码
        self.reg.api_img_code(123)
        # 获取短信的验证码
        print(self.reg.api_phone_code(phone, img_code).text)
        # 注册成功
        print(self.reg.api_register(phone, pwd, img_code, phone_code).text)
        # 登录成功
        print(self.reg.api_login(phone, pwd).text)
        # 认证成功
        self.approve.api_approve()
        # 后台开户
        response = self.approve.api_trust()
        print(response.text)
        # 三方开户
        result = parser_html(response)
        print(result)
        data = result[1]
        url = result[0]
        r = self.session.post(url, data)
        print(r.text)
        log.info("三方开户执行结果：{}".format(r.text))
        self.assertIn("OK", r.text)
        # 后台充值
        print(self.approve.api_recharge(img_code).text)
        self.reg.api_img_code(123)
        r = self.approve.api_recharge(img_code)
        # 10、三方充值
        result = parser_html(r)
        r = self.session.post(url=result[0], data=result[1])
        print("三方充值的结果为：", r.text)
        self.assertIn("OK", r.text)
        log.info("接口执行结果为：{}".format(r.text))
        # 11、后台投资
        r = self.tender.api_tender(100)
        # 12、三方投资
        result = parser_html(r)
        r = self.session.post(url=result[0], data=result[1])
        print("三方投资的结果为：", r.text)
        self.assertIn("OK", r.text)
        log.info("接口执行结果为：{}".format(r.text))


if __name__ == '__main__':
    unittest.man()

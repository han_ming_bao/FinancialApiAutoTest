import unittest
from time import sleep

from parameterized import parameterized
import requests
from ..uitl import read_json, clear_data
from ..api import log
from ..api.api_register_login import ApiRegesiterLogin


class TestRegesiterLogin(unittest.TestCase):
    # 类级别每次测试前执行一遍清除数据
    @classmethod
    def setUpClass(cls) -> None:
        pass
        # 数据库无法连接  暂时弃用
        # clear_data()

    def setUp(self) -> None:
        self.session = requests.session()
        self.reg = ApiRegesiterLogin(self.session)

    def tearDown(self) -> None:
        self.session.close()

    # 测试图片验证码
    @parameterized.expand(read_json("register_login.json", "img_code"))
    def test01_img_code(self, random, expect_code):
        try:
            print(random, expect_code)
            res = self.reg.api_img_code(random)
            log.info("执行结果{}".format(res.status_code))
            self.assertEqual(expect_code, res.status_code)
            log.info("断言成功")
            print("图片验证码测试", (res.status_code))
        except Exception as e:
            log.error("断言错误：{}".format(e))
            raise

    # 测试短信验证码
    @parameterized.expand(read_json("register_login.json", "phone_code"))
    def test02_phone_code(self, phone, imgVerifyCode, expect_text):
        try:
            # 图片验证码
            self.reg.api_img_code(123)
            # 短信验证码
            res = self.reg.api_phone_code(phone=phone, imgVerifyCode=imgVerifyCode)
            log.info("执行结果{}".format(res.text))
            self.assertIn(expect_text, res.text)
            log.info("断言成功")
            print(res.text)
        except Exception as e:
            log.error("断言错误：{}".format(e))

    # 测试注册
    @parameterized.expand(read_json("register_login.json", "register"))
    def test03_register(self, phone, password, imgVerifyCode, phone_code, expect_text):
        try:
            # 图片验证码
            self.reg.api_img_code(123)
            # 短信验证码
            self.reg.api_phone_code(phone=phone, imgVerifyCode=imgVerifyCode)
            # 注册
            res = self.reg.api_register(phone, password, imgVerifyCode, phone_code)
            log.info("执行结果{}".format(res.text))
            self.assertIn(expect_text, res.text)
            log.info("断言成功")
            print("注册测试", res.text)
        except Exception as e:
            log.error("断言错误：{}".format(e))

    # 测试登录
    @parameterized.expand(read_json("register_login.json", "login"))
    def test04_login(self, keywords, password, expect_text):
        # 测试锁定60秒之后登录成功(解锁成功)
        print(keywords, password, expect_text)
        try:
            res = None
            if "error" in password:
                for i in range(3):
                    res = self.reg.api_login(keywords, password)
                print("测试锁定")
                self.assertIn("锁定", res.text)
                sleep(60)
                res = self.reg.api_login("13600001111", "test123")
                self.assertIn(expect_text, res.text)
            else:
                res = self.reg.api_login(keywords, password)
                self.assertIn(expect_text, res.text)
                print(res.json())
            log.info("执行结果{}".format(res.text))
        except Exception as e:
            log.error("断言错误{}".format(e))

        # 测试登录状态接口

    @parameterized.expand(read_json("register_login.json", "login_status"))
    def test05_login_tatus(self, status, expect_text):
        try:
            if status == "已登录":
                self.reg.api_login("13600001111", "test123")
            response = self.reg.api_login_satus()
            log.info("断言错误{}".format(response.text))
            self.assertIn(expect_text, response.text)
            print(response.text)
        except Exception as e:
            log.error("断言错误：" + e)


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(TestRegesiterLogin("test04_login"))
    # unittest.main("TestRegesiterLogin")
    runner = unittest.TextTestRunner()
    runner.run(suite)

import requests
from parameterized import parameterized
import unittest
from ..api import log
from ..api.api_register_login import ApiRegesiterLogin
from ..api.api_tender import ApiTenDer
from ..uitl import read_json, parser_html


class TestTenDer(unittest.TestCase):

    def setUp(self) -> None:
        self.session = requests.session()
        self.tender = ApiTenDer(self.session)
        # 登录
        res = ApiRegesiterLogin(self.session).api_login("13299151967", "test123")
        # print(res.text)

    def tearDown(self) -> None:
        self.session.close()

    @parameterized.expand(read_json("tender.json", "tender"))
    def test01_tender(self, amount, expect_text):

        try:
            # 调用投资方法
            r = self.tender.api_tender(amount)
            print(r.text)
            if amount == 100:
                # 调用三方投资
                result = parser_html(r)
                r = self.session.post(url=result[0], data=result[1])
                print("三方投资的结果为：", r.text)
                log.info("接口执行结果为：{}".format(r.text))
                # 断言
                self.assertIn(expect_text, r.text)
            else:
                self.assertIn(expect_text, r.text)
        except Exception as e:
            log.error(e)
            raise


if __name__ == '__main__':
    unittest.main()

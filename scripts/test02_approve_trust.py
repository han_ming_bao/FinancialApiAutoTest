import unittest
import requests
from parameterized import parameterized
from ..api import log
from ..api.api_approve_trust import ApiApproveTrust
from ..api.api_register_login import ApiRegesiterLogin
from ..uitl import parser_html, read_json


class TestApproveTrust(unittest.TestCase):
    def setUp(self) -> None:
        self.session = requests.Session()
        self.approve = ApiApproveTrust(self.session)
        # 初始化seesion与登录之后才能开户
        ApiRegesiterLogin(self.session).api_login("13600001111", "test123")

    def tearDown(self) -> None:
        self.session.close()

    # 认证接口测试
    def test01_approve(self):
        try:
            response = self.approve.api_approve()
            log.info("执行结果：{}".format(response.text))
            self.assertIn("提交成功", response.text)
            log.info("断言通过！！")
            print(response.json())
        except Exception as e:
            log.error("错误断言{}".format(e))
            raise

    # 查询认证状态
    def test02_apprive_status(self):
        try:
            response = self.approve.api_approve_status()
            log.info("执行结果：{}".format(response.text))
            self.assertIn("华", response.text)
            log.info("断言通过！！")
            print(response.json())
        except Exception as e:
            log.error("错误断言{}".format(e))
            raise

    # 测试开户接口
    def test03_trust(self):
        try:

            response = self.approve.api_trust()
            log.info("执行结果：{}".format(response.json()))
            self.assertIn("form", response.text)
            log.info("断言通过！！")
            print(response.json())
            print("三方开户")
            result = parser_html(response)
            print(result)
            data = result[1]
            url = result[0]
            r = self.session.post(url, data)
            log.info("三方开户执行结果：{}".format(r.json()))
            self.assertIn("OK", r.text)
        except Exception as e:
            log.error("错误断言{}".format(e))
            raise

    # 获取验证码封装
    @parameterized.expand(read_json("approve_trust.json","img_code"))
    def test04_img_code(self, random,expect_text):
        try:
            response = self.approve.api_img_code(random)
            log.info("执行结果：{}".format(response.status_code))
            self.assertEqual(expect_text, response.status_code)
            # self.assertIn("from", response.text)
            log.info("断言通过！！")
            print(response.status_code)
        except Exception as e:
            log.error("错误断言{}".format(e))
            raise

    # 后台充值封装
    @parameterized.expand(read_json("approve_trust.json", "recharge"))
    def test05_recharge(self, valicode,expect_text):
        print(valicode,expect_text)
        try:
            # 依赖图片验证码
            self.approve.api_img_code(123)
            response = self.approve.api_recharge(valicode)
            if valicode==8888:

                self.assertIn("form", response.text)
                log.info("断言通过！！")
                print(response.json())
                print("三方充值")
                result = parser_html(response)
                print(result)
                data = result[1]
                url = result[0]
                r = self.session.post(url, data)
                print(r.text)
                log.info("三方充值执行结果：{}".format(r.text))
                self.assertIn("OK", r.text)
            else:
                self.assertIn(expect_text,response.text)
            log.info("执行结果：{}".format(response.json()))
        except Exception as e:
            log.error("错误断言{}".format(e))
            raise


if __name__ == '__main__':
    suite = unittest.TestSuite()
    suite.addTest(TestApproveTrust("test04_img_code"))
    # unittest.main("TestRegesiterLogin")
    runner = unittest.TextTestRunner()
    runner.run(suite)

from 金融自动化.config import HOST


class ApiRegesiterLogin:
    def __init__(self, session):
        self.session = session
        # 图片验证码
        self.__url_img_code = HOST + "/common/public/verifycode1/{}"
        # 短信验证码
        self.__url_phone_code = HOST + "/member/public/sendSms"
        # 注册
        self.__url_regiser = HOST + "/member/public/reg"
        # 登录
        self.__url_login = HOST + "/member/public/login"
        # 登录状态
        self.__url_login_status = HOST + "/member/public/islogin"

    # 图片验证码的封装
    def api_img_code(self, random):
        return self.session.get(self.__url_img_code.format(random))

    # 短信验证码的封装
    def api_phone_code(self, phone, imgVerifyCode):
        data = {"phone": phone,
                "imgVerifyCode": imgVerifyCode,
                "trpe": "reg"
                }
        return self.session.post(url=self.__url_phone_code, data=data)

    # 注册的封装
    def api_register(self, phone, password, verifycode, phone_code):
        data = {
            "invite_phone": "",
            "phone": phone,
            "password": password,
            "verifycode": verifycode,
            "phone_code": phone_code,
            "dy_server": "on"
        }
        return self.session.post(url=self.__url_regiser, data=data)

    # 登录的封装
    def api_login(self, keywords, password):
        data = {
            "keywords": keywords,
            "password": password
        }
        return self.session.post(url=self.__url_login, data=data)

    # 查询登录状态
    def api_login_satus(self):

        return self.session.post(url=self.__url_login_status)

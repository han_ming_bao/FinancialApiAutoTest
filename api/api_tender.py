# 投资api封装
from 金融自动化.config import HOST


class ApiTenDer:
    def __init__(self, session):
        self.session = session
        self.__url_tender = HOST + "/trust/trust/tender"

    # 投资方法
    def api_tender(self, account):
        data = {
            'id': 642,
            "depositCertificate": -1,
            "amount": account,
            "password": ""

        }
        return self.session.post(url=self.__url_tender, data=data)

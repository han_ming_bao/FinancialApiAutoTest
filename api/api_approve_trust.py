# 认证开户接口封装
from  ..config import HOST


class ApiApproveTrust:
    def __init__(self, session):
        # 初始化五个接口都要用的变量
        self.session = session
        # 认证url
        self.__url_approve = HOST + "/member/realname/approverealname"
        # 登录状态url
        self.__url_approve_status = HOST + "/member/member/getapprove"
        # 请求开户
        self.__url_trust = HOST + "/trust/trust/register"
        # 图片验证码
        self.__url_img_code = HOST + "/common/public/verifycode/{}"
        # 后台充值
        self.__url_recharge = HOST + "/trust/trust/recharge"

    # 认证接口封装
    def api_approve(self):
        data = {
            "realname": "华仔",
            "card_id": "360102199003072237"
        }
        return self.session.post(url=self.__url_approve, data=data, files={"x": "y"})

    # 查询登录状态封装
    def api_approve_status(self):
        return self.session.post(url=self.__url_approve_status)

    # 请求后台开户封装
    def api_trust(self):
        return self.session.post(url=self.__url_trust)

    #     获取验证码封装
    def api_img_code(self,random):
        return self.session.get(url=self.__url_img_code.format(random))

    #     后台充值封装
    def api_recharge(self,valicode):
        data={"paymentType":"chinapnrTrust",
              "amount":"1000",
              "formStr":"reForm",
              "valicode":valicode
              }
        return  self.session.post(url=self.__url_recharge,data=data)
